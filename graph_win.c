/*
Copyright (c) 2012 David Raffensperger

Permission is hereby granted, free of charge, to any person obtaining a copy 
of this software and associated documentation files (the "Software"), to deal 
in the Software without restriction, including without limitation the rights 
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is 
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
*/

/*
Windows API implementation of a few graph.h QuickC functions

See MSDN help for more info
http://msdn.microsoft.com/en-us/library/ms687407(v=vs.85).aspx
http://msdn.microsoft.com/en-us/library/ms686974(v=vs.85).aspx

*/

#include <windows.h>
#include "graph_win.h"

// Taken from: http://support.microsoft.com/kb/99261
void _clearscreen(short param) {
	//HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	//cls(hStdout);
	system("cls");
}

void _outtext(char * text)  {
	HANDLE hStdout;
	COORD co = {80, 80};
	DWORD numCharsWritten = 0;
	DWORD nNumberOfCharsToWrite = strlen(text);
	char* buf = text;

	//AllocConsole();
	hStdout = GetStdHandle(STD_OUTPUT_HANDLE);	
	//SetConsoleScreenBufferSize(hStdout, co);		

	//char* buf = (char*) malloc(sizeof(char) * nNumberOfCharsToWrite);
	//strncpy(buf, text, nNumberOfCharsToWrite);	
	
	// set output mode to ascii.
	// http://msdn.microsoft.com/en-us/library/windows/desktop/dd317756(v=vs.85).aspx
	SetConsoleOutputCP(20127);
	SetConsoleCP(20127);

	//WriteConsole(hStdout, buf, nNumberOfCharsToWrite, &numCharsWritten, NULL);

	//WriteConsole(hStdout, "hello", 5, &numCharsWritten, NULL);
	WriteFile(hStdout, buf, nNumberOfCharsToWrite, &numCharsWritten, NULL);

	//free(buf);
}

void _settextposition(short row, short col) {
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;	
	
	pos.X = col;
	pos.Y = row;
	SetConsoleCursorPosition(hStdout, pos);
}

/*

From: http://www.ousob.com/ng/msc60/ng9eb14.php
    No.   Color                No.   Color
    ---   ---------------      ---   ---------------
     0    Black                 8    Dark gray
     1    Blue                  9    Light blue
     2    Green                10    Light green
     3    Cyan                 11    Light cyan
     4    Red                  12    Light red
     5    Magenta              13    Light magenta
     6    Brown                14    Yellow
     7    White                15    Bright white

*/

short _settextcolor(short color) {
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	WORD attrib = 0;
	
	switch (color) {
	case 0:
		attrib = FOREGROUND_INTENSITY;
		break;
	case 1:
	case 9:
		attrib = FOREGROUND_BLUE;
		break;
	case 2:
	case 10:
		attrib = FOREGROUND_GREEN;
		break;
	case 3:
	case 11:
		attrib = FOREGROUND_BLUE;
		break;
	case 4:
	case 12:
		attrib = FOREGROUND_RED;
		break;
	case 5:
	case 13:
		attrib = FOREGROUND_RED;
		break;
	case 6:
	case 14:
		attrib = FOREGROUND_GREEN;
		break;
	case 7:
	case 15:
		attrib = FOREGROUND_INTENSITY;
		break;
	case 8:
		attrib = FOREGROUND_INTENSITY;
		break;
	default:
		attrib = FOREGROUND_INTENSITY;
		break;
	}
	
	SetConsoleTextAttribute(hStdout, attrib);
	return color;
}

short _settextcursor(short curtype) {
	CONSOLE_CURSOR_INFO curInfo;
	HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);				

	if (curtype == 0 || curtype == 0x2000) {
		curInfo.bVisible = FALSE;
		curInfo.dwSize = 1;
	} else {
		curInfo.bVisible = TRUE;
		curInfo.dwSize = 100;
	}
		
	SetConsoleCursorInfo (hStdout, &curInfo);

	return curtype;
}

short _setvideomode(short mode) {
	// do nothing, just prevent compile error;
	return 0;
}